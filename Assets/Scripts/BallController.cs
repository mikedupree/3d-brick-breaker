﻿using System.Runtime.Serialization;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Globalization;
using System.Runtime.CompilerServices;
using System;
using System.Collections;
using UnityEngine;

public class BallController : MonoBehaviour
{

    private Boolean wallReverse;
    private Boolean roofReverse;
    public float speed = 10;

    // Start is called before the first frame update
    void Start()
    {
        this.wallReverse = false;
        this.roofReverse = false;
    }

    // Update is called once per frame
   void Update()
    {
        float x = 1f;
        float y = 1f;

        // Move the object upward in world space 1 unit/second.
        if (this.wallReverse) {
            x = -x;
        }
         if (this.roofReverse) {
            y = -y;
        }

        Vector3 direction = new Vector3(x, y, 0f).normalized;
        transform.Translate(direction * Time.deltaTime * speed, Space.World);
    }

    void OnCollisionEnter(Collision col) {
        //TODO this doesnt handle if the ball hits the side of a brick.
        if (col.gameObject.tag == "Wall") {
            this.wallReverse = !this.wallReverse;
        }
        if (col.gameObject.tag == "Roof") {
            this.roofReverse = !this.roofReverse;
        }
    }
}
